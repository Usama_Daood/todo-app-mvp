package com.todo.www.todo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.todo.www.todo.Sharedprefrence.SharedPrefrence;
import com.todo.www.todo.Todo.Home_Todo;
import com.todo.www.todo.Todo.List.ListActivity;
import com.todo.www.todo.Utils.Methods;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import static android.provider.ContactsContract.Intents.Insert.EMAIL;

public class MainActivity extends AppCompatActivity {

    private AccessToken mAccessToken;
    private CallbackManager callbackManager;
    LoginButton loginButton;
    Context context;
    // Google Login

    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    RelativeLayout open_sign_up;
    String name, user_id,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        // FB Login Button is here.
        loginButton =  findViewById(R.id.login_details_fb_iV_id);
        loginButton.setReadPermissions(Arrays.asList(EMAIL));

        // Code to get Key hash

        Methods.printHashKey(context);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mAccessToken = loginResult.getAccessToken();
                getUserProfile(mAccessToken);

            }
            @Override
            public void onCancel() {
                Methods.toast_msg(MainActivity.this,"You cancel this.");
            }
            @Override
            public void onError(FacebookException error) {
                Methods.toast_msg(context,"" + error.toString());
            }
        });

        Button RL_fb = findViewById(R.id.fb_login);
        RL_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });

        /// Google Login

        try{
            google_sign_in();

        }catch (Exception v){

        }

        // Set the dimensions of the sign-in button.
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);

        Button gmail_sign_in = findViewById(R.id.gmail_login);

        gmail_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);

            }
        });




    }

    /// TODO: Handle Data if user login with Facebook.

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            String fb_user_id = object.getString("id");
                            String imgurl = "https://graph.facebook.com/"+object.getString("id")+"/picture";
                            String fb_name = object.getString("name");
                            String fb_email = object.getString("email");


                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("name", fb_name); // Set the first name/pair
                            jsonObj.put("email", fb_email);
                            jsonObj.put("user_id" , fb_user_id);
                            jsonObj.put("user_img", imgurl);

                            // Save data in shared preff.
                            SharedPrefrence.save_info_share(
                                    context,
                                    "" + jsonObj.toString(),
                                    "" + SharedPrefrence.shared_user_login_detail_key
                            );

                            LoginManager.getInstance().logOut();


                            open_Todo_page();
                            

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Methods.toast_msg(context,""+e.toString());
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(200)");
        request.setParameters(parameters);
        request.executeAsync();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode,  data);

            // Gmail lognin....
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }


    public void google_sign_in()
    {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .enableAutoManage(MainActivity.this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                }).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

    }

    // Handle result of Gmail Login

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            try{
                name = acct.getDisplayName();
                user_id = acct.getId();
                String img_url = "https://plus.google.com/s2/photos/profile/" +user_id + "?sz=100";
                email = acct.getEmail();

                JSONObject jsonObj_gmail = new JSONObject();
                jsonObj_gmail.put("name", name); // Set the first name/pair
                jsonObj_gmail.put("email", email);
                jsonObj_gmail.put("user_id" , user_id);
                jsonObj_gmail.put("user_img" , img_url);


                // Save data in shared preff.
                SharedPrefrence.save_info_share(
                        context,
                        "" + jsonObj_gmail.toString(),
                        "" + SharedPrefrence.shared_user_login_detail_key
                );


                open_Todo_page();

            }catch (Exception v){
                Methods.toast_msg(context,""+v.toString());

            }
        } else {


        }
    }


    // Open Todo Home Page

    public void open_Todo_page() {
        Intent myIntent = new Intent(MainActivity.this, ListActivity.class);
        startActivity(myIntent);
    }





}
