package com.todo.www.todo.Todo.Todo;

import android.content.Context;

import com.todo.www.todo.Todo.common.ParseDate;
import com.todo.www.todo.Todo.model.Entity.Todo;
import com.todo.www.todo.Todo.model.TodoRepository;
import com.todo.www.todo.Utils.Methods;

import java.util.Date;

public class TodoPresenter implements ITodoPresenter {

    private ITodoView view;
    private ITodoInteractor interactor;
    private Context context;

    private Todo editTodo;

    public TodoPresenter(ITodoView view, Context context) {
        this.view       = view;
        this.interactor = new TodoRepository(context.getContentResolver());
        this.context    = context;
    }

    @Override
    public void setEditTodo(Todo todo) {
        this.editTodo = todo;
        view.updateFields(todo.getTitle(), todo.getDescription(), todo.isCompleted());
        view.updateEditedField(ParseDate.parseDate(todo.getEdited(), ParseDate.HOUR_PATTERN));
    }

    @Override
    public void create(String title, String description, boolean completed) {
        if (!title.equals("") || !description.equals("")) {
            Todo todo = (this.editTodo != null) ? this.editTodo : new Todo();
            todo.setTitle(title);
            todo.setDescription(description);
            todo.setCompleted(completed);
            todo.setEdited(new Date().getTime());
            interactor.create(todo);
            Methods.toast_msg(context,"Saved");
        }
        view.finishView();
    }

    @Override
    public void delete() {
        if (editTodo != null) {
            interactor.delete(editTodo);
            Methods.toast_msg(context,"Deleted");
        }
        view.finishView();
    }

    @Override
    public void discard() {
        Methods.toast_msg(context,"Discarded");
        view.finishView();
    }

    @Override
    public void updateEditedTime() {
        view.updateEditedField(ParseDate.parseDate(null, ParseDate.HOUR_PATTERN));
    }



}
