package com.todo.www.todo.Todo;

import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.todo.www.todo.R;
import com.todo.www.todo.Utils.Methods;

public abstract class Home_Todo extends AppCompatActivity {
    public Toolbar getToolbar() {
        return (Toolbar) findViewById(R.id.toolbar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());

        if (getToolbar() != null) {
            setSupportActionBar(getToolbar());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_base, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:

                Methods.alert_dialogue(Home_Todo.this,"Info","" + getResources().getString(R.string.about));

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setToolbarTitle(@StringRes int title) {
        getToolbar().setTitle(title);
    }

    public void setBackButtonEnabled(boolean enabled) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(enabled);
            getSupportActionBar().setHomeButtonEnabled(enabled);
        }
    }

    public void setOnClickBackButton(View.OnClickListener listener) {
        getToolbar().setNavigationOnClickListener(listener);
    }

    protected abstract int getLayoutResource();



}
