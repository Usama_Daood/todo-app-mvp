package com.todo.www.todo.Todo.Todo;

import com.todo.www.todo.Todo.model.Entity.Todo;

public interface ITodoPresenter {

    /**
     * To.Do to edit is saved
     *
     * @param todo
     */
    void setEditTodo(Todo todo);

    /**
     * Create new To.Do or update an old one
     *
     * @param title
     * @param description
     * @param completed
     */
    void create(String title, String description, boolean completed);

    /**
     * Delete or discard To.Do
     */
    void delete();

    /**
     * Discard To.Do
     */
    void discard();

    /**
     * Get new Edited Time
     */
    void updateEditedTime();



}
