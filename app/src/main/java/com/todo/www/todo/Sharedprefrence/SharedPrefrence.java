package com.todo.www.todo.Sharedprefrence;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.todo.www.todo.MainActivity;
import com.todo.www.todo.Utils.Methods;

import org.json.JSONObject;

public class SharedPrefrence {

    public static SharedPreferences.Editor editor;
    public static SharedPreferences pref;
    public static String shared_user_login_detail_key = "user_info";


    public static void init_share(Context context){
        pref = context.getSharedPreferences("Todo_app", 0); // 0 - for private mode
        editor = pref.edit();
    }

    public static void save_info_share(Context context,String value,String data_key)
    {
        init_share(context);
        editor.putString(data_key, value); // Storing string
        editor.commit();
    }

    // Clear Key
    public static void remove_value_of_key (Context context, String key){
        init_share(context);
        boolean ch = pref.edit().remove(key).commit();
    }

    public static void remove_value (Context context,String remove_key){
        init_share(context);
        boolean ch = pref.edit().remove(remove_key).commit();
    }


    public static String get_offline(Context context, String key){
        init_share(context);
        pref.getString(key, null);
        // Variables.toast_msg(context,"Value "+pref.getString(key, null));
        return pref.getString(key, null);

    }

    public static String get_user_id_from_json(Context context){
        String user_json = get_offline(context,SharedPrefrence.shared_user_login_detail_key);
        // Get User Data from
        // Methods.toast_msg(context,"" + user_json);
        String user_id = "0";

        try{
            JSONObject user_obj = new JSONObject(user_json);

            user_id = user_obj.getString("id");
//            user.getString("first_name");
//            user.getString("email");
            // user_id = user.getString("id");

        }catch (Exception b){
            Methods.toast_msg(context,"" + b.toString());
        }

        return user_id;
    }


    ///////// ======= Get User Name
    public static String get_user_name_from_json (Context context){


        String user_json = get_offline(context,shared_user_login_detail_key);
        //  Methods.toast_msg(context,"" + user_json);
        // Get User Data from
        int user_id = 0;
        String user_name = "";
        try{
//            JSONObject response = new JSONObject(user_json);
//            JSONObject Arr = response.getJSONObject("msg");
//            JSONObject user = Arr.getJSONObject("User");
//            user_name = user.getString("username");

            JSONObject user_obj = new JSONObject(user_json);

            user_name = user_obj.getString("name");
            // email = user_obj.getString("email");

//            user.getString("email");
            //          user_id = user.getInt("id");

        }catch (Exception b){
            // Methods.toast_msg(context,"" + b.toString());
        }

        return user_name;
    }
    //////////// ========= ENd get User Name


    ///////// ======= Get User Name
    public static String get_user_img_from_json (Context context){
        String user_json = get_offline(context,shared_user_login_detail_key);
        //  Methods.toast_msg(context,"" + user_json);
        // Get User Data from
        int user_id = 0;
        String user_img = "";
        try{
            JSONObject user_obj = new JSONObject(user_json);

            user_img = user_obj.getString("user_img");
//            user.getString("first_name");
//            user.getString("email");
            // user_id = user.getString("id");

        }catch (Exception b){
            Methods.toast_msg(context,"" + b.toString());
        }

        return user_img;
    }

    ///////// ======= Get User Name
    public static String get_user_email_from_json (Context context){
        String user_json = get_offline(context,shared_user_login_detail_key);
        //  Methods.toast_msg(context,"" + user_json);
        // Get User Data from
        int user_id = 0;
        String email = "";
        try{
//            JSONObject response = new JSONObject(user_json);
//            JSONObject Arr = response.getJSONObject("msg");
//            JSONObject user = Arr.getJSONObject("User");
//            user_name = user.getString("username");

            JSONObject user_obj = new JSONObject(user_json);

            // user_name = user_obj.getString("full_name");
            email = user_obj.getString("email");

//            user.getString("email");
            //          user_id = user.getInt("id");

        }catch (Exception b){
            // Methods.toast_msg(context,"" + b.toString());
        }
        return email;
    }
    //////////// ========= ENd get User Name

}
