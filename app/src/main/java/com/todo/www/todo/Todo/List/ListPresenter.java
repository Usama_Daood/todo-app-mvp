package com.todo.www.todo.Todo.List;

import android.content.Context;

import com.todo.www.todo.R;
import com.todo.www.todo.Todo.model.Entity.Todo;
import com.todo.www.todo.Todo.model.TodoRepository;
import com.todo.www.todo.Utils.Methods;

import java.util.ArrayList;

public class ListPresenter implements IListPresenter  {

    private IListView view;
    private IListInteractor interactor;
    private Context context;

    public ListPresenter(IListView view, Context context) {
        this.view       = view;
        this.interactor = new TodoRepository(context.getContentResolver());
        this.context    = context;
    }

    @Override
    public void refreshSession() {
        view.setTodos(interactor.get());
        view.notifyListDataSetChanged();
    }

    @Override
    public void onAddTodoButtonClick() {
        view.showTodoView();
    }

    @Override
    public void onClickTodoItemToEdit(Todo todo) {
        view.showTodoViewToEdit(todo);
    }

    @Override
    public void onLongClickTodoItem(Todo todo) {
        CharSequence items[] = new CharSequence[] {
                context.getString(R.string.edit),
                context.getString(R.string.delete)
        };
        view.showItemDialog(todo, items);
    }

    @Override
    public void updateTodoIsCompleted(Todo todo, boolean completed, int position) {
        todo.setCompleted(completed);
        interactor.update(todo);
        ArrayList<Todo> todoList = interactor.get();
        view.notifyListItemRemoved(position);
        view.setTodos(todoList);
        for (Todo todoObject : todoList) {
            if (todoObject.getId() == todo.getId()) {
                view.notifyListItemInserted(todoList.indexOf(todoObject));
            }
        }
    }

    @Override
    public void delete(Todo todo) {
        interactor.delete(todo);
        view.setTodos(interactor.get());
        view.notifyListDataSetChanged();
        Methods.toast_msg(context,"Deleted");
    }

}
