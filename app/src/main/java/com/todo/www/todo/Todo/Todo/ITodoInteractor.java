package com.todo.www.todo.Todo.Todo;

import com.todo.www.todo.Todo.common.ITodoActionsInteractor;
import com.todo.www.todo.Todo.model.Entity.Todo;

public interface ITodoInteractor extends ITodoActionsInteractor {

    /**
     * Insert new To.Do in the database
     *
     * @param todo
     */
    void create(Todo todo);


}
