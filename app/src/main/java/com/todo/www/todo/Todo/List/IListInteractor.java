package com.todo.www.todo.Todo.List;

import com.todo.www.todo.Todo.common.ITodoActionsInteractor;
import com.todo.www.todo.Todo.model.Entity.Todo;

import java.util.ArrayList;

public interface IListInteractor extends ITodoActionsInteractor {

    ArrayList<Todo> get();

    /**
     * Update old To.Do
     *
     * @param todo
     */
    void update(Todo todo);

}
