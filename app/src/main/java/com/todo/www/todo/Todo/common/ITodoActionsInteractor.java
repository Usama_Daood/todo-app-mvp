package com.todo.www.todo.Todo.common;

import com.todo.www.todo.Todo.model.Entity.Todo;

public interface ITodoActionsInteractor {

    void delete(Todo todo);

}
