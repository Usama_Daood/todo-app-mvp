package com.todo.www.todo.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.todo.www.todo.R;
import com.todo.www.todo.Sharedprefrence.SharedPrefrence;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.facebook.AccessTokenManager.TAG;

public class Methods {
    public static void toast_msg(Context context, String msg){
        // Toast Msg
        Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();
        // Log for Debugging
        Log.d("Log","Log: \n" + msg); // :-D

    }

    /// Alert Dialogue
    public static void alert_dialogue(final Context context, String title, String msg) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle("" + title);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }
                });

//        builder1.setNegativeButton(
//                "No",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });

        AlertDialog alert11 = builder1.create();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            AlertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

            alert11.show();

        }else{
            alert11.show();
        }


    }

    // Get fb Keyhash

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }


    public static void getName_and_display (Activity activity, Context context) {
        TextView name  = activity.findViewById(R.id.name);
        ImageView image = activity.findViewById(R.id.user_img);
        // Get data from Json and Display Profile.
        SharedPrefrence.get_user_name_from_json(context);
        name.setText("" + SharedPrefrence.get_user_name_from_json(context));

        // Set up image

        Picasso.get()
                .load(SharedPrefrence.get_user_img_from_json(context))
                .placeholder(R.drawable.ic_account)
                .error(R.drawable.ic_account)
                .into(image);



    }




}
